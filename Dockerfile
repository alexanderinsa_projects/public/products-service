FROM openjdk:11-jdk-slim as build
WORKDIR /
COPY src /src
COPY gradle /gradle
COPY gradlew /gradlew
COPY build.gradle.kts /build.gradle.kts
COPY settings.gradle.kts /settings.gradle.kts
RUN /gradlew build

FROM openjdk:11-jdk-slim
ARG JAR_FILE=build/libs/*.jar
COPY --from=build ${JAR_FILE}  /app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app.jar"]