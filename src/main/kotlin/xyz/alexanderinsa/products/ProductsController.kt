package xyz.alexanderinsa.products

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
class ProductsController {

    @GetMapping("/")
    fun index(): String = "Greetings from Kotlin Spring Boot!"
}

    
