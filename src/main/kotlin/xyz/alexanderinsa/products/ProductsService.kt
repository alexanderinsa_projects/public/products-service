package xyz.alexanderinsa.products

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ProductsService

fun main(args: Array<String>) {
	runApplication<ProductsService>(*args)
}
